import React from 'react';

const ItemList = (props) => {
    return (
        <ul className="list-group">
          {
            props.articles.map(article => <li className="list-group-item" key={article.id}>{article.quantity} <b>{article.name}</b></li>)
          }
        </ul>
    );
}

export default ItemList;
