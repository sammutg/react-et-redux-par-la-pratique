import React, { Component } from 'react';


class Form extends Component {

    state = {
        name: '',
        quantity: 0,
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state);
        this.props.addArticle(this.state);
        this.setState({name: '', quantity: 0});
    }

    render() {
        return(
            <div>
                <h3>{ this.props.formTitle }</h3>
                <form onSubmit={this.handleSubmit} className="form-inline">
                  <div className="form-group">

                    <input
                        type="number"
                        placeholder="quantité"
                        className="form-control"
                        value={this.state.quantity}
                        onChange={ (event) => this.setState({quantity: event.target.value }) } />

                    <input
                        type="text"
                        placeholder="article"
                        className="form-control"
                        value={this.state.name}
                        onChange={ (event) => this.setState({name: event.target.value}) } />

                    <button type="submit" className="btn btn-primary">
                      Ajouter
                    </button>

                  </div>
                </form>
            </div>
        );
    }
}

// console.log(event)}
//
// {this.state.name}
// value="{this.state.quantity}"
//
//
// (event) => this.setState({ name: event.target.value })
// (event) => this.setState({ quantity: event.target.value }) } />
//
// const Form = () => {
//     return (
//         <div>Form Component</div>
//     );
// }

export default Form;
