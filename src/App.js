import React, { Component } from 'react';

import Header from './components/Header';
import Form from './components/Form';
import ItemList from './components/ItemList';
import Footer from './components/Footer';

import logo from './logo.svg';
import './App.css';

class App extends Component {

  state = { articles : []};

  addArticle = (article) => {
    let oldArticles = this.state.articles;
    article.id = Date.now();
    let newArticles = [...oldArticles, article];
    this.setState({ articles: newArticles });
    // console.log('article vu par le parent app', article);
  }

  render() {
    return (
      <div className="App container">

        <Header />

        <div className="header clearfix"></div>

        <div className="jumbotron">
          <p className="lead">Pour démarrer, éditez <code>src/App.js</code> et enregistrez pour rafraichir.</p>
        </div>

        <Form formTitle="Ajouter un article" addArticle={this.addArticle} />
          <h2>Liste de course</h2>
        <ItemList articles={this.state.articles} />

        <img src={logo} className="App-logo" alt="logo" />

        <Footer />

      </div>
    );
  }
}

export default App;
